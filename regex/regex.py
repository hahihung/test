import re
def test(string):
    regex_python = r'(.*[^<?php][^\{\};]\n)'
    regex_CSharp = r'(using System)| (namespace)'
    regex_CPlusPlus = r'(#include).'
    regex_PHP = r'(<\?php)'
    if re.search(regex_PHP, string, re.M | re.I):
        return 'PHP'
    elif re.search(regex_CSharp, string, re.M | re.I):
        return 'C#'
    elif re.search(regex_CPlusPlus, string, re.M|re.I):
        return 'C++'
    elif re.search(regex_python, string, re.M | re.I):
        return 'Python'
    else:
        return 'Java'

for x in range(0, 5):
    # open file
    file = open('input' + str(x) + '.txt', 'r')
    #put data into string
    string = file.read()
    file.close()
    result = test(string)
    print (test(string))



